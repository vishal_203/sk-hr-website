import React from "react";
import Brochure from "../assest/sk-hr-brochure.pdf"

export const Header = (props) => {
  return (
    <header id="header">
      <div className="intro">
        <div className="overlay">
          <div className="container">
            <div className="row">
              <div className=" col-md-8 col-md-offset-2 intro-text">
                <h1>
                  {props.data ? props.data.title : "Loading"}
                  <span></span>
                </h1>
                <p>{props.data ? props.data.paragraph : "Loading"}</p>
                <a
                  href={Brochure} download
                  className="btn btn-custom btn-lg page-scroll me-1"
                >
                  To know more
                </a>
                <a
                  href="#contact"
                  className="btn btn-custom btn-lg page-scroll"
                >
                  Let's Talk
                </a>{" "}
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};
